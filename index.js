/**
 * Componente WeatherApi
 *
 * Copyright(c) Climatempo
 * MIT Licensed
 *
 * @author Michel Araujo <araujo_michel@yahoo.com.br>
 */

var http = require('http');
var request = require('request');
var querystring = require('querystring');

var Routes = require('./Routes');
var Cache = require('./Cache');

"use strict";

/**
 * Construtor WeatherApi
 *
 * @author Michel Araujo <araujo_michel@yahoo.com.br>
 * @constructor
 */
function WeatherApi(authConfigApi) {
    this._httpConfg = {};
    this._cache = null;
    this.routes = new Routes();

    this._pathCache = authConfigApi.pathCache;
    delete authConfigApi.pathCache;
    this._authConfigApiRaw = authConfigApi;
    this._authConfigApi = querystring.stringify(authConfigApi);

    this._host = null;
    if (typeof authConfigApi.host !== undefined) {
        this._host = authConfigApi.host;
        this._scheme = authConfigApi.httpScheme;
        delete authConfigApi.host;
    }
}

/**
 * Constantes com os métodos HTTP suportados na API
 *
 * @type {string}
 */
WeatherApi.prototype.POST = 'POST';

WeatherApi.prototype.GET = 'GET';

/**
 * Constantes gerais
 */
WeatherApi.prototype.MAX_REQUEST_RETRY = 5

/**
 * Configura os parâmetros para fazer a requisição HTTP
 *
 * @param {string} method Método HTTP que será feita a requisição
 * @param {string} url Recurso que será feita a requisição
 * @author Michel Araujo <araujo_michel@yahoo.com.br>
 * @returns {WeatherApi}
 */
WeatherApi.prototype.from = function from(method, url) {
    this._httpConfg = {
        host: this._host ? this._host : 'api.climatempo.com.br',
        port: 80,
        path: '/api/v1/' + url,
        method: method,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': Buffer.byteLength(this._authConfigApi)
        }
    };

    return this;
};

/**
 * Configura a query string da rota seguindo os parâmetros passado para essa função
 *
 * @param {Object} params Json com os parâmetros que vai gerar a query string
 * @author Michel Araujo <araujo_michel@yahoo.com.br>
 * @returns {WeatherApi}
 */
WeatherApi.prototype.params = function params(params) {
    this._httpConfg.path = this._httpConfg.path + '?' + querystring.stringify(params) + '&token=' + Buffer.from(this._authConfigApi).toString('base64');
    
    return this;
};

/**
 * Envia dados via post
 *
 * @payload {Object} Json com os dados
 * @author Leonardo Carvalho <leonardo.carvalho@climatempo.com.br>
 * @returns {WeatherApi}
 */
WeatherApi.prototype.post = function post(payload) {
  this._httpConfg.path = this._httpConfg.path + '?' + 'token=' + Buffer.from(this._authConfigApi).toString('base64');

  return new Promise((resolve, reject) => {
    const options = {
      method: this._httpConfg.method,
      url: `${this._scheme}://${this._httpConfg.host}${this._httpConfg.path}`,
      headers: this._httpConfg.headers,
      form: { ...payload, ...this._authConfigApiRaw }
    };

    delete options.headers['Content-Length'];

    request(options, function (error, response, body) {
      if (error) reject(error);
      resolve(JSON.parse(body));
    });
  });
};

/**
 * Instancia o objeto Cache com as configurações passada pelo usuário
 *
 * @param {Object} config Configurações passada pelo usuário
 * @author Michel Araujo <araujo_michel@yahoo.com.br>
 * @returns {WeatherApi}
 */
WeatherApi.prototype.cache = function (config) {
    this._cache = new Cache(config, this._pathCache);
    return this;
};

/**
 * Se não tiver cache chama o request e salva o seu retorno
 * se tiver cache somente retorna os dados do arquivo de cache
 *
 * @callback callback
 * @author Michel Araujo <araujo_michel@yahoo.com.br>
 * @returns {WeatherApi}
 */
WeatherApi.prototype.done = function done(callback) {
    var WeatherApi = this,
        cacheName = this._httpConfg.path;

    this._cache.isExpired(cacheName, function (isExpired) {
        if (isExpired) {

            WeatherApi._request(function (results) {
                if (WeatherApi._pathCache !== undefined) {
                    WeatherApi._cache.save(cacheName, results);
                }

                callback(results);
            });

        } else {
            WeatherApi._cache.get(cacheName, function (results) {
                callback(results);
            });
        }
    });
};

/**
 * Faz a requisição HTTP e retorna os dados da rota relacionada
 *
 * @param callback
 * @param retry Número de tentativas feitas
 * @author Michel Araujo <araujo_michel@yahoo.com.br>
 * @private
 */
WeatherApi.prototype._request = function request(callback, retry = 1) {
    var results = '',
    WeatherApi = this;

    if (retry >= this.MAX_REQUEST_RETRY) return callback(results)
    var postHttp = http.request(this._httpConfg, function(response) {
        response.setEncoding('utf8');

        response.on('data', function (data) {
            results += data;
        });

        response.on('end', function() {
            try {
                if (JSON.parse(results)) {
                    callback(results);
                }
            } catch(err) {
                WeatherApi._request(callback, ++retry);
            }
        });
        response.on('error', function(e) {
            WeatherApi._request(callback, ++retry);
        });
    })
    .on("error", (err) => {
        WeatherApi._request(callback, ++retry)
    });

    postHttp.write(this._authConfigApi);
    postHttp.end();
};

module.exports = WeatherApi;
