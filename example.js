/**
 * Arquivo para fazer alguns testes
 *
 * @author Michel Araujo <araujo_michel@yahoo.com.br>
 */
var express = require('express')();
var WeatherApi = require('./WeatherApi');

var wp = new WeatherApi({
    'key': '671',
    'client': 'consultoria',
    'nocache': true,
    'type':'json',
    'application': 'teste',
    'pathCache': './cacheTeste/'
});

/**
 * BO 01
 * @param callback
 */
var OffshoreBO = function (callback) {
    wp.from(wp.POST, wp.routes.FORECAST_OFFSHORE_HOURLY_WAVE)
        .params({"idLocale":83915, "dateBegin":"2016-09-23", "dateEnd":"2016-09-25"})
        .cache({time:2})
        .done(function(result) {
            callback(result);
        });
};

/**
 * BO 02
 * @param callback
 */
var DaysOffshore = function (callback) {
    wp.from(wp.POST, wp.routes.FORECAST_15DAYS_TEMPERATURE)
        .params({"idLocale":83915})
        .cache({time:2})
        .done(function(result) {
            callback(result);
        });
};

/**
 * Controller 01
 */
express.get('/', function(req, res) {
    var data = [];

    OffshoreBO(function (results) {
        data.push(JSON.parse(results));

        DaysOffshore(function (results) {
            data.push(JSON.parse(results));
            res.send(data);
        });
    });
});

/**
 * Sobe a zuera!
 */
express.listen(3000, function() {
    console.log("Servidor rodando...");
});