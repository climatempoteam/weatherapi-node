/**
 * Componente WeatherApi
 *
 * Copyright(c) Climatempo
 * MIT Licensed
 *
 * @author Michel Araujo <araujo_michel@yahoo.com.br>
 */

var fs = require('fs');

"use strict";

/**
 * Construtor Cache
 *
 * @param {Object} config Json com as configurações de cache (Tempo de cache é em minutos)
 * @param {Object} pathCache
 * @author Michel Araujo <araujo_michel@yahoo.com.br>
 * @constructor
 */
function Cache(config, pathCache) {
    this._config = config;

    if (!this._config.time) {
        this._config.time = 15; //15 Minutos
    }

    this._config.path = pathCache;
}

/**
 * Verifica se o arquivo de cache está com a data expirada e retorna true ou false para o callback
 *
 * Uma peculiaridade sobre o fs.stat é que ele retorna um erro quando o arquivo não existe
 * sendo assim criei essas validação “if (error.message.indexOf('no such file or directory')) {“
 * para fazer o retorno adequado
 *
 * OBS: Se não for passado um diretório de cache não vai ter cache
 *
 * @callback callback
 *
 * @param {string} cacheName URI da rota que vai se transformar no nome do arquivo de cache para a mesma
 * @param callback
 * @author Michel Araujo <araujo_michel@yahoo.com.br>
 */
Cache.prototype.isExpired = function (cacheName, callback) {
  if (this._config.path === undefined) {
    callback(true);
  } else {
    if (this._config.time === -1) {
      callback(true);
    } else {
      var cacheTime = this._config.time,
        file = this._getName(cacheName);

      fs.stat(file, function(error, stats) {
        if (!error) {
          if (stats != undefined) {
            var dateCurrent = new Date(),
              lastModified = stats.mtime,
              diff = dateCurrent - lastModified,
              minutes = Math.round((diff/1000)/60);

            if (minutes >= cacheTime) {
              callback(true);
            } else {
              callback(false);
            }
          }
        } else {
          if (error.message.indexOf('no such file or directory')) {
            callback(true);
          }

          callback(false);
        }
      });
    }
  }
};

/**
 * Salva o conteúdo no arquivo de cache
 *
 * @param {string} cacheName URI da rota que vai se transformar no nome do arquivo de cache para a mesma
 * @param {Object} data Conteúdo em json para ser armazenado no cache
 * @author Michel Araujo <araujo_michel@yahoo.com.br>
 */
Cache.prototype.save = function (cacheName, data) {
    fs.writeFile(this._getName(cacheName), data, function(err) {
        if (err) {
            console.log('Erro ao salvar o arquivo');
        }
    });
};

/**
 * Retorna o conteúdo do cache
 *
 * @callback callback
 *
 * @param {string} cacheName URI da rota que vai se transformar no nome do arquivo de cache para a mesma
 * @param callback
 * @author Michel Araujo <araujo_michel@yahoo.com.br>
 */
Cache.prototype.get = function (cacheName, callback) {
    fs.readFile(this._getName(cacheName), function (error, data) {
        if (!error) {
            callback(data);
        }
    });
};

/**
 * Formata a URI para ser usada como nome do arquivo de cache
 *
 * @param {string} name URI da rota que vai se transformar no nome do arquivo de cache para a mesma
 * @returns {string}
 * @author Michel Araujo <araujo_michel@yahoo.com.br>
 * @private
 */
Cache.prototype._getName = function (name) {
    var cacheName = name.split('/').join("_");
    cacheName = cacheName.split('=').join("_");
    cacheName = cacheName.split('?').join("_");
    cacheName = cacheName.split('&').join("_");

    return this._config.path+cacheName+'.json';
};

module.exports = Cache;