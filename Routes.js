/**
 * Componente WeatherApi
 * arquivo que define as constantes com as rotas
 *
 * Copyright(c) Climatempo
 * MIT Licensed
 */

/**
 * Construtor Routes
 * @constructor
 */
function Routes() {
}

/**
 * Define as rotas
 * @type {string}
 */

Routes.prototype.LIST_TIMEZONES = 'timezone';
Routes.prototype.USER = 'user';
Routes.prototype.MODULE_LOCALES = 'module/locales';
Routes.prototype.MODULES = 'modules';
Routes.prototype.PRODUCTS = 'products';

Routes.prototype.LOCALE = 'locale';
Routes.prototype.LOCALES_STATES = 'locales/states';
Routes.prototype.LOCALES_CITIES = 'locales/cities';
Routes.prototype.LOCALES_OFFSHORE = 'locales/offshore';

/**
 * Rotas de previsão diária
 */
Routes.prototype.FORECAST_BIGPLAN = 'forecast/bigplan';
Routes.prototype.FORECAST_PERIOD_DAILY = 'forecast/period/daily';
Routes.prototype.FORECAST_BEACH_PERIOD_DAILY = 'forecast/beach/period/daily';
Routes.prototype.FORECAST_PERIOD_ICON = 'forecast/period/icon';
Routes.prototype.FORECAST_15DAYS_ICON = 'forecast/15days/icon';
Routes.prototype.FORECAST_PERIOD_HUMIDITY = 'forecast/period/humidity';
Routes.prototype.FORECAST_15DAYS_HUMIDITY = 'forecast/15days/humidity';
Routes.prototype.FORECAST_PERIOD_RAIN = 'forecast/period/rain';
Routes.prototype.FORECAST_PERIOD_NEXTMONTH_RAIN = 'forecast/period/nextmonth-rain';
Routes.prototype.FORECAST_15DAYS_RAIN = 'forecast/15days/rain';
Routes.prototype.FORECAST_PERIOD_TEMPERATURE_GROUND = 'forecast/period/temperature/ground';
Routes.prototype.FORECAST_PERIOD_TEMPERATURE = 'forecast/period/temperature';
Routes.prototype.FORECAST_15DAYS_TEMPERATURE = 'forecast/15days/temperature';
Routes.prototype.FORECAST_PERIOD_WIND = 'forecast/period/wind';
Routes.prototype.FORECAST_15DAYS_WIND = 'forecast/15days/wind';
Routes.prototype.FORECAST_PERIOD_RADIATION = 'forecast/period/radiation';
Routes.prototype.FORECAST_PERIOD_WAVE = 'forecast/period/wave';
Routes.prototype.FORECAST_PERIOD_PRESSURE = 'forecast/period/pressure';
Routes.prototype.FORECAST_15DAYS_PRESSURE = 'forecast/15days/pressure';
Routes.prototype.FORECAST_PERIOD_UV = 'forecast/period/uv';
Routes.prototype.FORECAST_15DAYS_UV = 'forecast/15days/uv';
Routes.prototype.FORECAST_PERIOD_THERMAL_SENSATION = 'forecast/period/thermal-sensation';
Routes.prototype.FORECAST_15DAYS_THERMAL_SENSATION = 'forecast/15days/thermal-sensation';
Routes.prototype.FORECAST_PERIOD_MOON = 'forecast/period/moon';
Routes.prototype.FORECAST_15DAYS_MOON = 'forecast/15days/moon';
Routes.prototype.FORECAST_PERIOD_SUN = 'forecast/period/sun';
Routes.prototype.FORECAST_15DAYS_SUN = 'forecast/15days/sun';
Routes.prototype.FORECAST_PERIOD_MOSQUITO = 'forecast/period/mosquito';
Routes.prototype.FORECAST_PERIOD_RISK = 'forecast/period/risk';
Routes.prototype.FORECAST_PERIOD_LIGHTNING_RISK = 'forecast/period/lightning/risk';
Routes.prototype.FORECAST_15DAYS_LIGHTNING_RISK = 'forecast/15days/lightning/risk';
Routes.prototype.FORECAST_PERIOD_CLOUD = 'forecast/period/cloud';
Routes.prototype.FORECAST_PERIOD_WATER_GROUND = 'forecast/period/water/ground';
Routes.prototype.FORECAST_REGION = 'forecast/region';
Routes.prototype.FORECAST_15DAYS_VISIBILITY = 'forecast/15days/visibility';
Routes.prototype.FORECAST_PERIOD_VISIBILITY = 'forecast/period/visibility';
Routes.prototype.FORECAST_15DAYS_FLOWRATE = 'forecast/15days/flowrate';
Routes.prototype.FORECAST_PERIOD_DAILY_LONG = 'forecast/period/daily/long';

/**
 * Rotas de previsão horária
 */
Routes.prototype.FORECAST_PERIOD_HOURLY = 'forecast/period/hourly';
Routes.prototype.FORECAST_HOURLY_PRESSURE = 'forecast/hourly/pressure';
Routes.prototype.FORECAST_72HOURS_PRESSURE = 'forecast/72hours/pressure';
Routes.prototype.FORECAST_HOURLY_WIND = 'forecast/hourly/wind';
Routes.prototype.FORECAST_72HOURS_WIND = 'forecast/72hours/wind';
Routes.prototype.FORECAST_HOURLY_TEMPERATURE = 'forecast/hourly/temperature';
Routes.prototype.FORECAST_72HOURS_TEMPERATURE = 'forecast/72hours/temperature';
Routes.prototype.FORECAST_HOURLY_UV = 'forecast/hourly/uv';
Routes.prototype.FORECAST_72HOURS_UV = 'forecast/72hours/uv';
Routes.prototype.FORECAST_HOURLY_RAIN = 'forecast/hourly/rain';
Routes.prototype.FORECAST_72HOURS_RAIN = 'forecast/72hours/rain';
Routes.prototype.FORECAST_HOURLY_HUMIDITY = 'forecast/hourly/humidity';
Routes.prototype.FORECAST_72HOURS_HUMIDITY = 'forecast/72hours/humidity';
Routes.prototype.FORECAST_72HOURS_VISIBILITY = 'forecast/72hours/visibility';
Routes.prototype.FORECAST_HOURLY_LIGHTNING_RISK = 'forecast/hourly/lightning/risk';
Routes.prototype.FORECAST_HOURLY_LIGHTNING_RISK_EDIT = 'forecast/hourly/lightning/risk/edit';
Routes.prototype.FORECAST_HOURLY_HAIL_RISK = 'forecast/hourly/hail/risk';
Routes.prototype.FORECAST_HOURLY_CLOUD = 'forecast/hourly/cloud';
Routes.prototype.FORECAST_HOURLY_CLOUD_EDIT = 'forecast/hourly/cloud/edit';
Routes.prototype.FORECAST_HOURLY_TSM = 'forecast/hourly/tsm';

/**
 * Rotas de previsão horária offshore
 */
Routes.prototype.FORECAST_OFFSHORE_HOURLY_WAVE = 'forecast/offshore/hourly/wave';
Routes.prototype.FORECAST_OFFSHORE_HOURLY_WAVE_EDIT = 'forecast/offshore/hourly/wave/edit';
Routes.prototype.FORECAST_OFFSHORE_72HOURS_WAVE = 'forecast/offshore/72hours/wave';
Routes.prototype.FORECAST_OFFSHORE_HOURLY_WIND = 'forecast/offshore/hourly/wind';
Routes.prototype.FORECAST_OFFSHORE_HOURLY_WIND_EDIT = 'forecast/offshore/hourly/wind/edit';
Routes.prototype.FORECAST_OFFSHORE_72HOURS_WIND = 'forecast/offshore/72hours/wind';
Routes.prototype.FORECAST_OFFSHORE_HOURLY_RAIN = 'forecast/offshore/hourly/rain';
Routes.prototype.FORECAST_OFFSHORE_HOURLY_RAIN_EDIT = 'forecast/offshore/hourly/rain/edit';
Routes.prototype.FORECAST_OFFSHORE_72HOURS_RAIN = 'forecast/offshore/72hours/rain';
Routes.prototype.FORECAST_OFFSHORE_HOURLY_TEMPERATURE = 'forecast/offshore/hourly/temperature';
Routes.prototype.FORECAST_OFFSHORE_HOURLY_TEMPERATURE_EDIT = 'forecast/offshore/hourly/temperature/edit';
Routes.prototype.FORECAST_OFFSHORE_72HOURS_TEMPERATURE = 'forecast/offshore/72hours/temperature';
Routes.prototype.FORECAST_OFFSHORE_HOURLY_VISIBILITY = 'forecast/offshore/hourly/visibility';
Routes.prototype.FORECAST_OFFSHORE_HOURLY_VISIBILITY_EDIT = 'forecast/offshore/hourly/visibility/edit';
Routes.prototype.FORECAST_OFFSHORE_72HOURS_VISIBILITY = 'forecast/offshore/72hours/visibility';
Routes.prototype.FORECAST_OFFSHORE_HOURLY_PRESSURE = 'forecast/offshore/hourly/pressure';
Routes.prototype.FORECAST_OFFSHORE_HOURLY_PRESSURE_EDIT = 'forecast/offshore/hourly/pressure/edit';
Routes.prototype.FORECAST_OFFSHORE_72HOURS_PRESSURE = 'forecast/offshore/72hours/pressure';
Routes.prototype.FORECAST_OFFSHORE_HOURLY_HUMIDITY = 'forecast/offshore/hourly/humidity';
Routes.prototype.FORECAST_OFFSHORE_72HOURS_HUMIDITY = 'forecast/offshore/72hours/humidity';
Routes.prototype.FORECAST_OFFSHORE_HOURLY_ICON = 'forecast/offshore/hourly/icon';
Routes.prototype.FORECAST_OFFSHORE_HOURLY_ICON_EDIT = 'forecast/offshore/hourly/icon/edit';
Routes.prototype.FORECAST_OFFSHORE_72HOURS_ICON = 'forecast/offshore/72hours/icon';
Routes.prototype.FORECAST_OFFSHORE_HOURLY_SWELLWINDSEA = 'forecast/offshore/hourly/swellwindsea';
Routes.prototype.FORECAST_OFFSHORE_72HOURS_SWELLWINDSEA = 'forecast/offshore/72hours/swellwindsea';
Routes.prototype.FORECAST_OFFSHORE_HOURLY_OCEAN_CURRENT = 'forecast/offshore/hourly/ocean-current';
Routes.prototype.FORECAST_OFFSHORE_72HOURS_OCEANCURRENT = 'forecast/offshore/72hours/ocean-current';
Routes.prototype.FORECAST_72HOURS_SUNSHINE_DURATION = 'forecast/72hours/sunshine-duration';

/**
 * Rotas de ativação dos dados horários offshore
 */
Routes.prototype.FORECAST_OFFSHORE_HOURLY_WAVE_ACTIVE = 'forecast/offshore/hourly/wave/active';
Routes.prototype.FORECAST_OFFSHORE_HOURLY_WIND_ACTIVE = 'forecast/offshore/hourly/wind/active';
Routes.prototype.FORECAST_OFFSHORE_HOURLY_WIND50M_ACTIVE = 'forecast/offshore/hourly/wind50m/active';
Routes.prototype.FORECAST_OFFSHORE_HOURLY_RAIN_ACTIVE = 'forecast/offshore/hourly/rain/active';
Routes.prototype.FORECAST_OFFSHORE_HOURLY_TEMPERATURE_ACTIVE = 'forecast/offshore/hourly/temperature/active';
Routes.prototype.FORECAST_OFFSHORE_HOURLY_VISIBILITY_ACTIVE = 'forecast/offshore/hourly/visibility/active';
Routes.prototype.FORECAST_OFFSHORE_HOURLY_PRESSURE_ACTIVE = 'forecast/offshore/hourly/pressure/active';
Routes.prototype.FORECAST_OFFSHORE_HOURLY_HUMIDITY_ACTIVE = 'forecast/offshore/hourly/humidity/active';
Routes.prototype.FORECAST_OFFSHORE_HOURLY_ICON_ACTIVE = 'forecast/offshore/hourly/icon/active';
Routes.prototype.FORECAST_OFFSHORE_HOURLY_SWELLWINDSEA_ACTIVE = 'forecast/offshore/hourly/swellwindsea/active';
Routes.prototype.FORECAST_OFFSHORE_HOURLY_CURRENT_ACTIVE = 'forecast/offshore/hourly/ocean-current/active';

/**
 * Rotas de previsão de dados georreferenciados
 */
Routes.prototype.FORECAST_GEO_DAILY = 'geo/forecast/daily';
Routes.prototype.FORECAST_GEO_HOURLY = 'geo/forecast/hour';
Routes.prototype.FORECAST_GEO_PHYTOSANITARY = 'geo/phytosanitary';
Routes.prototype.FORECAST_GEO_DAILY_AGRICULTURAL = 'geo/forecast/daily-agriculture';
Routes.prototype.GEO_SPRAY_WINDOW_HOURLY = 'geo/spray-window-hourly-by-period'
Routes.prototype.GEO_MAP = 'geo/map';
Routes.prototype.GEO_FIRE = 'geo/fire';
Routes.prototype.GEO_CITY = 'geo/city';

/**
 * Rotas de previsão climática
 */
Routes.prototype.CLIMATE = 'climate';
Routes.prototype.CLIMATE_FLOWRATE = 'climate/flowrate';
Routes.prototype.CLIMATE_RAIN = 'climate/rain';
Routes.prototype.CLIMATE_TEMPERATURE = 'climate/temperature';
Routes.prototype.CLIMATE_DAILY_RAIN = 'climate/daily/rain';
Routes.prototype.CLIMATE_DAILY_TEMPERATURE = 'climate/daily/temperature';
Routes.prototype.CLIMATE_REGION = 'climate/region';
Routes.prototype.CLIMATE_TSM = 'climate/tsm';

/**
 * Rotas de histórico
 */
Routes.prototype.HISTORY_RADIATION = 'history/radiation';
Routes.prototype.HISTORY_RAIN = 'history/rain';
Routes.prototype.HISTORY_TEMPERATURE = 'history/temperature';
Routes.prototype.HISTORY_WIND = 'history/wind';
Routes.prototype.HISTORY_HUMIDITY = 'history/humidity';
Routes.prototype.HISTORY_PERIOD_DAILY = 'history/period/daily';
Routes.prototype.HISTORY_HOURLY_RAIN = 'history/hourly/rain';
Routes.prototype.HISTORY_LIGHTNING = 'history/lightning';
Routes.prototype.HISTORY_ALERT = 'history/alert';

/**
 * Rotas relacionadas à notícias
 */
Routes.prototype.NEWS = 'news';
Routes.prototype.NEWS_SLUG = 'news/slug';
Routes.prototype.NEWS_CATEGORIES_TAGS = 'news/categories-tags';
Routes.prototype.NEWS_CATEGORIES = 'news/categories';
Routes.prototype.NEWS_TAGS = 'news/tags';
Routes.prototype.NEWS_BUSCA = 'news/busca';
Routes.prototype.NEWS_TIPO = 'news/tipo';
Routes.prototype.NEWS_NOTICIA = 'news/noticia';
Routes.prototype.NEWS_PERIOD = 'news/period';
Routes.prototype.NEWS_CALLS = 'news/calls';
Routes.prototype.NEWS_FEATURED = 'news/featured';

Routes.prototype.MARINE_WAVE = 'marine/wave';
Routes.prototype.MARINE_TIDE = 'marine/tide';

Routes.prototype.SATELLITE_IMAGES = 'satellite/images';
Routes.prototype.SATELLITE_RADAR = 'satellite/radar';
Routes.prototype.SATELLITE_TEXT = 'satellite/text';
Routes.prototype.SATELLITE_FORECAST = 'satellite/forecast';
Routes.prototype.SATELLITE_AGRICULTURAL = 'satellite/agricultural';

Routes.prototype.MONITORING_LOWEST_TEMPERATURE = 'monitoring/lowest-temperature';
Routes.prototype.MONITORING_WEATHER = 'monitoring/weather';
Routes.prototype.MONITORING_SEASON = 'monitoring/season';
Routes.prototype.MONITORING_LOCALE = 'monitoring/locale';
Routes.prototype.MONITORING_LIGHTNING = 'monitoring/lightning';
Routes.prototype.MONITORING_LIGHTNING_CEP = 'monitoring/lightning/cep';
Routes.prototype.ALERT_LIGHTNING_CEP = 'alert/lightning/cep';
Routes.prototype.ALERTA_RAIO_CEP = 'alerta/raio/cep';
Routes.prototype.ALERT_SEND_PERIODS = 'alert/send-periods';
Routes.prototype.ALERT_CONTACT = 'alert/contact';
Routes.prototype.ALERT_CONTACTS = 'alert/contacts';
Routes.prototype.ALERT_CONTACTS_SEARCH = 'alert/contacts/search';
Routes.prototype.ALERT_CONTACTS_SEARCH_BULK = 'alert/contacts/search/bulk';
Routes.prototype.ALERT_CONTACT_PERSIST = 'alert/contact/persist';
Routes.prototype.ALERT_CONTACT_PERSIST_BULK = 'alert/contact/persist/bulk';
Routes.prototype.ALERT_CONTACT_DELETE = 'alert/contact/delete';
Routes.prototype.ALERT_SENDCONFIG_SEARCH = 'alert/sendconfig/search';
Routes.prototype.ALERT_SENDCONFIG_PERSIST = 'alert/sendconfig/persist';
Routes.prototype.ALERT_CONFIG_PERSIST = 'alert/config/persist';
Routes.prototype.ALERT_SENDCONFIG_DELETE = 'alert/sendconfig/delete';
Routes.prototype.ALERT_LIMITS = 'alert/limits';

Routes.prototype.FILES = 'files';
Routes.prototype.FILES_DOWNLOAD = 'files/download';

Routes.prototype.DANGER = 'danger';

Routes.prototype.NEARBY_CITIES = 'nearby/cities';
Routes.prototype.NEARBY_BEACHES = 'nearby/beaches';

Routes.prototype.CROPS_LOCALE = 'crops/locale';

Routes.prototype.AUDITING = 'auditing';

Routes.prototype.RANKING_LOWER_TEMPERATURE_MIN = 'ranking/lower/temperature/min';
Routes.prototype.RANKING_LOWER_TEMPERATURE_MAX = 'ranking/lower/temperature/max';

Routes.prototype.HISTORY_RECORD_TEMPERATURE_MAX = 'history/record/temperature/max';

/**
 * Rotas grupo de email
 * @type {string}
 */
Routes.prototype.EMAILS = 'emails';
Routes.prototype.EMAILS_MODULE = 'emails/module';
Routes.prototype.EMAILS_GROUP_REMOVE = 'emails/email-groups/remove';
Routes.prototype.EMAILS_GROUP_CREATE = 'emails/email-groups/create';

/**
 * Rotas da Central de Pagamento
 */
Routes.prototype.PAYMENT_AUTH = 'payment/user/auth';
Routes.prototype.PAYMENT_REGISTER = 'payment/register';
Routes.prototype.PAYMENT_CHARGE = 'payment/charge';
Routes.prototype.PAYMENT_SUBSCRIPTION = 'payment/subscription';
Routes.prototype.PAYMENT_SUBSCRIPTION_CREATE = 'payment/subscription/create';
Routes.prototype.PAYMENT_SUBSCRIPTION_CANCELED = 'payment/subscription/canceled';
Routes.prototype.PAYMENT_SUBSCRIPTION_CANCEL = 'payment/subscription/cancel';
Routes.prototype.PAYMENT_SUBSCRIPTION_UPGRADE_PLAN = 'payment/subscription/upgrade-plan-subscription';
Routes.prototype.PAYMENT_BILL_CUSTOMER_SUBSCRIPTION = 'payment/bill/customer/subscription';
Routes.prototype.PAYMENT_BILL_CUSTOMER_SEPARATED = 'payment/bill/customer/separated';
Routes.prototype.PAYMENT_BILL_DETAIL = 'payment/bill/detail';
Routes.prototype.PAYMENT_USER_CUSTOMER = 'payment/user/customer';
Routes.prototype.PAYMENT_USER_EDIT = 'payment/user/edit';
Routes.prototype.PAYMENT_USER = 'payment/user';
Routes.prototype.PAYMENT_USER_ALL = 'payment/user/all';
Routes.prototype.PAYMENT_USER_CHANGE_PASSWORD = 'payment/user/password';
Routes.prototype.PAYMENT_USER_PASSWORD_RESET = 'payment/user/password/send';
Routes.prototype.PAYMENT_USER_PASSWORD_CHECK = 'payment/user/password/check';
Routes.prototype.PAYMENT_USER_PASSWORD_CHANGE_TOKEN = 'payment/user/password/change';
Routes.prototype.PAYMENT_USER_ADDRESS_EDIT = 'payment/user/address/edit';
Routes.prototype.PAYMENT_PAYMENT_PROFILE = 'payment/payment-profile';
Routes.prototype.PAYMENT_PAYMENT_PROFILE_CREATE = 'payment/payment-profile/create';
Routes.prototype.PAYMENT_PAYMENT_PROFILE_REMOVE = 'payment/payment-profile/remove';
Routes.prototype.PAYMENT_PAYMENT_SUPPORT = 'payment/support';
Routes.prototype.PAYMENT_CUSTOMERS_SUBSCRIPTIONS = 'payment/customer/subscription';
Routes.prototype.PAYMENT_USER_TOKEN_GENERATE = 'payment/user/token/generate';
Routes.prototype.PAYMENT_USER_FILTER = 'payment/user/filter';
Routes.prototype.PAYMENT_COUPON_LIST = 'payment/coupon';
Routes.prototype.PAYMENT_COUPON_CREATE = 'payment/coupon/create';
Routes.prototype.PAYMENT_COUPON_EDIT = 'payment/coupon/edit';
Routes.prototype.PAYMENT_COUPON_EDIT_STATUS = 'payment/coupon/edit-status';
Routes.prototype.PAYMENT_COUPON_FIND = 'payment/coupon/find';
Routes.prototype.PAYMENT_COUPON_USE_CHECK = 'payment/coupon/use/check';
Routes.prototype.PAYMENT_PRODUCT_LIST = 'payment/products';
Routes.prototype.PAYMENT_PLAN_LIST = 'payment/plans';
Routes.prototype.PAYMENT_GATEWAY_PLAN = 'payment/gateway/plans';
Routes.prototype.PAYMENT_GATEWAY_PAYMENT_METHODS = 'payment/gateway/payment-methods';
Routes.prototype.PAYMENT_SUBSCRIPTION_UPDATE = 'payment/subscription/update';
Routes.prototype.PAYMENT_SUBSCRIPTION_RENOVATE = 'payment/subscription/renovate';

/**
 * Rotas Marketing RdStation
 */
Routes.prototype.MARKETING_LEAD_CREATE = 'marketing/lead/create';

/**
 * Rotas de dados observados
 */
Routes.prototype.OBSERVED_LATEST_STATION = 'observed/latest';
Routes.prototype.OBSERVED_LATEST_STATION_NEAREST = 'observed/latest/nearest-station';

/**
 * Rotas agroclimapro
 * @type {string}
 */
Routes.prototype.GEO_AGROCLIMAPRO_POINT_BYUSER = 'geo/agroclimapro/point/by-user';
Routes.prototype.GEO_AGROCLIMAPRO_POINT_CREATE = 'geo/agroclimapro/point/create';
Routes.prototype.GEO_RADAR_STATISTICS = 'geo/radar/statistics';
Routes.prototype.GEO_AGROCLIMAPRO_FARM_BYUSER = 'geo/agroclimapro/farm/by-user';
Routes.prototype.GEO_AGROCLIMAPRO_FILED_BYFARM = 'geo/agroclimapro/field/by-farm';
/**
 * Rotas do projeto TVS
 */
Routes.prototype.TVS_AUTH = 'tvs/auth';
Routes.prototype.TVS_VERIFY_EMAIL = 'tvs/auth/verify/email';
Routes.prototype.TVS_USER_EDIT = 'tvs/user/edit';
Routes.prototype.TVS_USER_INSERT = 'tvs/user/insert';
Routes.prototype.TVS_LOCALES = 'tvs/locales';
Routes.prototype.TVS_LOCALES_INSERT = 'tvs/locales/insert';

/**
 * Rota de texto
 */
Routes.prototype.REGION_TEXT = 'text/region';
Routes.prototype.BRAZIL_ANALYSIS_TEXT = 'text/analysis/brazil';
Routes.prototype.BRAZIL_FEATURED_TEXT = 'text/featured/brazil';
Routes.prototype.STATE_TEXT = 'text/state';


/**
 * Rotas de cargos
 */
Routes.prototype.OCCUPATION_FIND_BY = 'user/position-held';
Routes.prototype.OCCUPATION_DELETE = 'user/position-held/delete';
Routes.prototype.OCCUPATION_EDIT = 'user/position-held/edit';
Routes.prototype.OCCUPATION_CREATE = 'user/position-held/create';

/**
 * Rotas de  grupos
 */
Routes.prototype.GROUP_CREATE = 'user/group/create';
Routes.prototype.GROUP_EDIT = 'user/group/edit';
Routes.prototype.GROUP_DELETE = 'user/group/delete';
Routes.prototype.GROUP_FIND_BY = 'user/group/find-by';


/**
 * Rotas de locales group
 */
Routes.prototype.GROUP_LOCALES_LIST = 'user/groupLocales/list';
Routes.prototype.GROUP_LOCALES_DELETE = 'user/groupLocales/delete';
Routes.prototype.GROUP_LOCALES_EDIT = 'user/groupLocales/edit';
Routes.prototype.GROUP_LOCALES_CREATE = 'user/groupLocales/create';
Routes.prototype.GROUP_LOCALES_LIST_LOCALES = 'user/groupLocales/locales/list'
Routes.prototype.GROUP_LOCALES_CASCADE = 'user/groupLocales';

/**
 * Rotas de permissões
 */
Routes.prototype.PERMISSION_GROUP_SAVE = 'user/permission-group/save';
Routes.prototype.PERMISSION_GROUP_DELETE = 'user/permission-group/delete';

/**
 * Rotas statistic
 */
Routes.prototype.CONTACT_STATISTIC = 'alert/contact/statistic';

/**
 * Rotas de Contact
 */
Routes.prototype.CONTACT_FIND = 'alert/contacts';
Routes.prototype.CONTACT_DELETE = 'alert/contact/delete';
Routes.prototype.CONTACT_SAVE = 'alert/contact/persist';
Routes.prototype.CONTACT_PHONE_UPDATE = 'alert/contact/phone/update';
Routes.prototype.CONTACT_PHONE_DELETE = 'alert/contact/phone/delete';
Routes.prototype.CONTACT_EMAIL_UPDATE = 'alert/contact/email/update';
Routes.prototype.CONTACT_EMAIL_DELETE = 'alert/contact/email/delete';
Routes.prototype.CONTACT_CONFIG_FIND = 'alert/contacts/config';
Routes.prototype.ALERT_SENDCONFIG_UPDATE = 'alert/sendconfig/update';
Routes.prototype.ALERT_SENDCONFIG_DELETE_CONTACT = 'alert/contact/sendconfig/delete';
Routes.prototype.ALERT_CONTACT_CELL = 'alert/contact/cell';

/**
 * Rota para listar módulos de permissão
 */
Routes.prototype.PRODUCT_MODULES = 'user/product-module/find-by';

/**
 * Rotas de Language
 */
Routes.prototype.LANGUAGE = 'language';

/**
 * Rotas de usuários (produtos)
 */
Routes.prototype.USER_CREATE = 'user/create';
Routes.prototype.USER_EDIT = 'user/edit';
Routes.prototype.USER_DELETE = 'user/delete';
Routes.prototype.USER_LIST = 'order/users';
Routes.prototype.USER_GENERATE_TOKEN = 'user/generate-token';

Routes.prototype.ALERT_LOG_COUNT = 'alert/log/count';
Routes.prototype.ALERT_LOGS = 'alert/logs';
Routes.prototype.ALERT_LOGS_PERSIST = 'alert/logs/persist';
Routes.prototype.ALERT_LOGS_DETAILS = 'alert/logs/details';

Routes.prototype.MODULE = 'module';

module.exports = Routes;
